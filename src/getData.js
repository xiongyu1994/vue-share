

import request from './assets/http'

function gamehomedata(data) {
    return request({
        url: '/game/find',
        method: 'get',
        data
    })
}

export default {
    gamehomedata
}