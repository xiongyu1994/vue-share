import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Music from '@/components/Music'
import Good from '@/components/Good'
import Index from '../components/Index'

Vue.use(Router)
console.log("Index",Index)


export default new Router({
  routes: [
    {
      path: '/',
      // name: 'Index',
      redirect:'/music',
      component: Music,
      children:[
        {
          component:Good,
          path:"good",
          name:"good",
          meta:{
            keepAlive:false,
            pageview:{
              page_id:"0000",
              page_name:"商品",
              page_comment:"武汉商品"
    
            }
          }
        },
        {
          component:Music,
          path:"music",
          name:"music",
          meta:{
            keepAlive:true,
            title:'音乐',
            pageview:{
              page_id:"0001",
              page_name:"音乐",
              page_comment:"武汉音乐"
    
            }
          }
        }
       
      ]
      
    },
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
