// axios的封装 为了可以方便的统一的使用axios做数据请求
// axios是原生ajax的封装
import promise from 'es6-promise'
promise.polyfill()
import axios from 'axios'
import qs from "qs"
import Vue from 'vue'

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
// create an axios instance
// axios.defaults.withCredentials = false;
const service = axios.create({
    withCredentials: true,
    //baseURL: 'http://app-v091.1216.top',
    baseURL:'http://192.168.1.3:8080/api',
    // baseURL: 'https://app-v09x.woaihaoyouxi.com',
    timeout: 8000, // request timeout
})




// request interceptor
service.interceptors.request.use(config => {
    //config.headers['JSESSIONID'] = sessionStorage.getItem("logintoken")
    config.headers['Content-Type'] = 'multipart/form-data'
    config.data = qs.stringify(config.data)
    
    
    return config
}, error => {
    // Do something with request error
    // console.log(error) // for debug
    Promise.reject(error)
})

// respone interceptor   拦截器
service.interceptors.response.use(
    // response => response,
    /**
     * 下面的注释为通过在response里，自定义code来标示请求状态
     * 当code返回如下情况则说明权限有问题，登出并返回到登录页
     * 如想通过xmlhttprequest来状态码标识 逻辑可写在下面error中
     * 以下代码均为样例，请结合自生需求加以修改，若不需要，则可删除
     */
    response => {
        window.console.log("成功",response)
        const res = response.data
        res.retcode !== 0 && Vue.$toast(res.retinfo)
        res.retcode === 30020001 && (window.location.href = './')
        return res
    },
    error => {
        // console.log('err' + error) // for debug
        window.console.log("err",error)
        return Promise.reject(error)
    }
)

function http(config) {
    // 根据记录的登录信息在参数中默认加入用户信息
    if (config.method.toLowerCase() === 'post') {
        // let _data = qs.stringify(config.data, { arrayFormat: 'repeat', allowDots: true });
        // config.data = sessionStorage.getItem("logintoken") ? {JSESSIONID:sessionStorage.getItem("logintoken"),..._data} : _data
    } else {
        config.params = sessionStorage.getItem("logintoken") ? {JSESSIONID:sessionStorage.getItem("logintoken"),...config.data} :config.data;
    }
    return service(config);
}

export default http;