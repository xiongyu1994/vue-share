/**
 * 微信js-sdk
 * 参考文档：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141115
 */
//import Vue from "vue";
import wx from 'weixin-js-sdk'
import axios from "axios"

let urlData = encodeURIComponent(window.location.href.split('#')[0])
console.log("urlData",urlData)
const wxApi = {
  /**
  * [wxRegister 微信Api初始化]
  * @param  {Function} callback [ready回调函数]
  * 
  */

  wxRegister(callback){
    wx.config({
      debug: true, // 开启调试模式
      appId:  'wxc7f546bb2da73c70',     //res.data.result.appId, // 必填，公众号的唯一标识
      timestamp: 1583829587,  //res.data.result.timestamp, // 必填，生成签名的时间戳
      nonceStr: "c2c860d5b7584ba5beb85b2e19af77ae",    //res.data.result.noncestr, // 必填，生成签名的随机串
      signature:"b8a89828add9e146a0f8e17db691a2b5a6aa66d4",    //res.data.result.signature, // 必填，签名，见附录1
      jsApiList: [
          "updateAppMessageShareData",//自定义“分享给朋友”及“分享到QQ”按钮的分享内容
          "updateTimelineShareData",//自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容
          "onMenuShareWeibo",//获取“分享到腾讯微博”按钮点击状态及自定义分享内容接口
      ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    })
    wx.ready((res) => {
      // 如果需要定制ready回调方法
      if (callback) {
        callback()
      }
    })
  },
  /** 
  wxRegister (callback) {
    // 这边的接口请换成你们自己的
    axios.get('http://app-v091.1216.top/app/wx_signature', { reqUrl: window.location.href.split('#')[0] }, { timeout: 5000, withCredentials: true }).then((res) => {
      console.log("res,,,",res)
      //let data = JSON.parse(res.data.data) // PS: 这里根据你接口的返回值来使用
      wx.config({
        debug: true, // 开启调试模式
        appId: res.data.result.appId, // 必填，公众号的唯一标识
        timestamp: res.data.result.timestamp, // 必填，生成签名的时间戳
        nonceStr: res.data.result.noncestr, // 必填，生成签名的随机串
        signature: res.data.result.signature, // 必填，签名，见附录1
        jsApiList: [
            "updateAppMessageShareData",//自定义“分享给朋友”及“分享到QQ”按钮的分享内容
            "updateTimelineShareData",//自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容
            "onMenuShareWeibo",//获取“分享到腾讯微博”按钮点击状态及自定义分享内容接口
        ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
      })
    }).catch((error) => {
      console.log(error)
    })
    wx.ready((res) => {
      // 如果需要定制ready回调方法
      if (callback) {
        callback()
      }
    })
  },*/
  /**
  * [ShareTimeline 微信分享到朋友圈]
  * @param {[type]} option [分享信息]
  * @param {[type]} success [成功回调]
  * @param {[type]} error   [失败回调]
  */
  ShareTimeline (option) {
    wx.updateTimelineShareData({
      title: option.title, // 分享标题
      link: option.link, // 分享链接
      imgUrl: option.imgUrl, // 分享图标
      success () {
        // 用户成功分享后执行的回调函数
        option.success()
      },
      cancel () {
        // 用户取消分享后执行的回调函数
        option.error()
      }
    })
  },
  /**
  * [ShareAppMessage 微信分享给朋友]
  * @param {[type]} option [分享信息]
  * @param {[type]} success [成功回调]
  * @param {[type]} error   [失败回调]
  */
  ShareAppMessage (option) {
    wx.updateAppMessageShareData({
      title: option.title, // 分享标题
      desc: option.desc, // 分享描述
      link: option.link, // 分享链接
      imgUrl: option.imgUrl, // 分享图标
      success () {
        console.log("哈哈，分享成功！！！")
        // 用户成功分享后执行的回调函数
        option.success()
      },
      cancel () {
        // 用户取消分享后执行的回调函数
        option.error()
      }
    })
  }
}
export default wxApi