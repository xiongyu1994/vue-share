import NativeShare from 'nativeshare'
// let isUC = navigator.userAgent.indexOf('UCBrowser') > -1
// let isQQ = new RegExp('MQQBrowser').test(navigator.userAgent)
// 如果你需要在微信浏览器中分享，那么你需要设置额外的微信配置
// 特别提示一下微信分享有一个坑，不要分享安全域名以外的链接(具体见jssdk文档)，否则会导致你配置的文案无效
// 创建实例应该带参数

//var nativeShare = new NativeShare()
var nativeShare = new NativeShare({
    wechatConfig: {
        appId: 'wxc7f546bb2da73c70',
        timestamp: "1583981979",
        nonceStr: "fefc1386bed241bd87d4111c627a50181",
        signature: "1d1a786a3464a32a63f8e4b7f1b90de063521eea",
    },
    // 让你修改的分享的文案同步到标签里，比如title文案会同步到<title>标签中
    // 这样可以让一些不支持分享的浏览器也能修改部分文案，默认都不会同步
    syncDescToTag: false,
    syncIconToTag: false,
    syncTitleToTag: false,
})

// 你也可以在setConfig方法中设置配置参数
nativeShare.setConfig({
    wechatConfig: {
        appId: 'wxc7f546bb2da73c70',
        timestamp: "1583981979",
        nonceStr: "fefc1386bed241bd87d4111c627a50181",
        signature: "1d1a786a3464a32a63f8e4b7f1b90de063521eea",
    }
})


// 设置分享文案
nativeShare.setShareData({
    icon: 'https://pic3.zhimg.com/v2-080267af84aa0e97c66d5f12e311c3d6_xl.jpg',
    link: 'http://app-h5.1216.top/#/gamesay?id=0-61e5b-0k40ro01e',
    title: '好游戏',
    desc: '一款好玩的游戏',
    from: '中传互动',
})

    // 唤起浏览器原生分享组件(如果在微信中不会唤起，此时call方法只会设置文案。类似setShareData) 
    try {
        nativeShare.call()
        //default 默认，调用起底部的分享组件，当其他命令不支持的时候也会调用该命令
        //wechatTimeline 分享到朋友圈
        //wechatFriend 分享给微信好友
        //qqFriend 分享给QQ好友
        //qZone 分享到QQ空间
        //weibo 分享到微博

        //如果是分享到微信则需要 nativeShare.call('wechatFriend')
        //类似的命令下面有介绍
    } catch(err) {
        // 如果不支持，你可以在这里做降级处理
    }
    
    
    
export default nativeShare
